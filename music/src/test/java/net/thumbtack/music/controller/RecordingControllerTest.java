package net.thumbtack.music.controller;

import net.thumbtack.music.dto.RecordingDto;
import net.thumbtack.music.service.RecordingService;
import net.thumbtack.music.exception.ExceptionDtoResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.HttpStatus;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = RecordingController.class)
class RecordingControllerTest {
    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private RecordingService mockRecordingService;

    @Test
    void testSaveRecording() throws Exception {
        final RecordingDto dto = new RecordingDto(
                "Portishead",
                "Glory Box",
                "Dummy",
                1994,
                "/url/portishead_dummy_600.jpg",
                "electronic",
                "5:08",
                "/url/audio",
                null
        );
        mvc.perform(
                post("/api/storage")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(dto))
        )
                .andExpect(status().isOk());
    }

    @Test
    void testSaveRecordingFailed() throws Exception {
        final RecordingDto dto = new RecordingDto(
                null,
                "Glory Box",
                "Dummy",
                1994,
                "/url/portishead_dummy_600.jpg",
                "electronic",
                "5:08",
                "/url/audio",
                null
        );
        MvcResult result = mvc.perform(
                post("/api/storage")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(dto))
        )
                .andReturn();

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), result.getResponse().getStatus());

        final ExceptionDtoResponse error = mapper.readValue(
                result.getResponse().getContentAsString(),
                ExceptionDtoResponse.class
        );
        assertTrue(error
                .getErrorList()
                .get(0)
                .contains("artist")
        );
    }

    @Test
    void testSaveRecordingFailed_dtoHasAllErrors() throws Exception {
        final RecordingDto dto = new RecordingDto(
                null,
                null,
                "Dummy",
                1960,
                "/url/portishead_dummy_600.jpg",
                null,
                null,
                null,
                null
        );

        final MvcResult result = mvc.perform(
                post("/api/storage")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(dto))
        )
                .andReturn();
        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), result.getResponse().getStatus());

        final ExceptionDtoResponse error = mapper.readValue(
                result.getResponse().getContentAsString(),
                ExceptionDtoResponse.class
        );
        assertEquals(6, error.getErrorList().size());
    }
}