package net.thumbtack.music.controller;

import net.thumbtack.music.dto.PublicationDtoRequest;
import net.thumbtack.music.dto.PublicationDtoResponse;
import net.thumbtack.music.dto.RecordingDto;
import net.thumbtack.music.service.PublicationService;
import net.thumbtack.music.exception.ExceptionDtoResponse;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = PublicationController.class)
class PublicationControllerTest {
    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private PublicationService mockPublicationService;

    @Test
    void testPublishTrack() throws Exception {
        final RecordingDto recordingDto = new RecordingDto(
                "Portishead",
                "Glory Box",
                "Dummy",
                1994,
                "/url/portishead_dummy_600.jpg",
                "electronic",
                "5:08",
                "/url/audio",
                null
        );
        final PublicationDtoRequest request = new PublicationDtoRequest(
                recordingDto,
                Arrays.asList("itunes", "youtubemusic"),
                ZonedDateTime.now().truncatedTo(ChronoUnit.SECONDS)
        );

        final PublicationDtoResponse response = new PublicationDtoResponse(UUID.randomUUID().toString());
        when(mockPublicationService.publish(any(PublicationDtoRequest.class)))
                .thenReturn(response);

        final MvcResult result = mvc.perform(
                post("/api/channels")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(request))
        )
                .andReturn();

        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
        final PublicationDtoResponse actualResponse = mapper.readValue(
                result.getResponse().getContentAsString(),
                PublicationDtoResponse.class
        );
        assertEquals(response.getPublicationId(), actualResponse.getPublicationId());
    }

    @Test
    void testTrackPublicationFailed() throws Exception {
        final RecordingDto recordingDto = new RecordingDto(
                "Portishead",
                "Glory Box",
                "Dummy",
                1994,
                "/url/portishead_dummy_600.jpg",
                "electronic",
                "5:08",
                "/url/audio",
                null
        );
        final PublicationDtoRequest request = new PublicationDtoRequest(
                recordingDto,
                Arrays.asList("itunes", "zunemarketplace"),
                ZonedDateTime.now().truncatedTo(ChronoUnit.SECONDS)
        );

        final MvcResult result = mvc.perform(
                post("/api/channels")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(request))
        )
                .andReturn();

        assertEquals(HttpStatus.INTERNAL_SERVER_ERROR.value(), result.getResponse().getStatus());

        final ExceptionDtoResponse error = mapper.readValue(
                result.getResponse().getContentAsString(),
                ExceptionDtoResponse.class
        );
        assertTrue(error
                .getErrorList()
                .get(0)
                .contains("publicationChannels")
        );
    }

    @Test
    void testDeletePublication() throws Exception {
        mvc.perform(
                delete("/api/channels/" + UUID.randomUUID().toString())
        )
                .andExpect(status().isOk());
    }
}