package net.thumbtack.music.controller;

import net.thumbtack.music.dto.PromotionDto;
import net.thumbtack.music.service.promotion.PromotionService;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MvcResult;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = PromotionController.class)
class PromotionControllerTest {
    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private PromotionService mockPromotionService;

    @Test
    void testStartCampaign() throws Exception {
        final PromotionDto promotionDto = new PromotionDto(UUID.randomUUID().toString());
        when(mockPromotionService.startCampaign(anyString()))
                .thenReturn(promotionDto);

        final MvcResult result = mvc.perform(
                post("/api/track/" + UUID.randomUUID().toString() + "/promo")
        )
                .andReturn();

        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());

        final PromotionDto response = mapper.readValue(
                result.getResponse().getContentAsString(),
                PromotionDto.class
        );
        assertEquals(promotionDto.getPromotionCampaignId(), response.getPromotionCampaignId());
    }

    @Test
    void testStopCampaign() throws Exception {
        mvc.perform(
                put("/api/track/" + UUID.randomUUID().toString()
                        + "/promo/" + UUID.randomUUID().toString()
                )
        )
                .andExpect(status().isOk());
    }

    @Test
    void testDeleteCampaign() throws Exception {
        mvc.perform(
                delete("/api/track/" + UUID.randomUUID().toString()
                        + "/promo/" + UUID.randomUUID().toString()
                )
        )
                .andExpect(status().isOk());
    }
}