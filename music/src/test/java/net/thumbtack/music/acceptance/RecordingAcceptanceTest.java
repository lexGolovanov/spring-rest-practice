package net.thumbtack.music.acceptance;

import net.thumbtack.music.dto.RecordingDto;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
public class RecordingAcceptanceTest {
    private RestTemplate template = new RestTemplate();

    @Test
    void testSaveRecording() {
        final RecordingDto dto = new RecordingDto(
                "Portishead",
                "Glory Box",
                "Dummy",
                1994,
                "/url/portishead_dummy_600.jpg",
                "electronic",
                "5:08",
                "/url/audio",
                null
        );

        final RecordingDto response = template.postForObject(
                "http://localhost:8080/api/storage",
                dto,
                RecordingDto.class
        );

        assertAll(
                () -> assertEquals(dto.getArtist(), response.getArtist()),
                () -> assertEquals(dto.getSongTitle(), response.getSongTitle()),
                () -> assertEquals(dto.getAlbumTitle(), response.getAlbumTitle()),
                () -> assertEquals(dto.getYear(), response.getYear()),
                () -> assertEquals(dto.getAlbumCoverUrl(), response.getAlbumCoverUrl()),
                () -> assertEquals(dto.getGenre(), response.getGenre()),
                () -> assertEquals(dto.getLength(), response.getLength()),
                () -> assertNotEquals(dto.getAudioUrl(), response.getAudioUrl())
        );
    }

    @Test
    void testSaveRecordingFail() {
        final RecordingDto dto = new RecordingDto(
                "Portishead",
                "Glory Box",
                "Dummy",
                1337,
                "/url/portishead_dummy_600.jpg",
                "electronic",
                "5:08",
                "/url/audio",
                null
        );

        try {
            final RecordingDto response = template.postForObject(
                    "http://localhost:8080/api/storage",
                    dto,
                    RecordingDto.class
            );
        } catch (HttpServerErrorException e) {
            assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, e.getStatusCode());
            assertTrue(e.getResponseBodyAsString().contains("year"));
        }
    }
}
