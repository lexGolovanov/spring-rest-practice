package net.thumbtack.music.storage;

import net.thumbtack.music.config.StorageUrlProperties;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

@Service("videoStorage")
public class VideoStorage implements DataStorage {
    private final String videoStorageUrl;

    @Autowired
    public VideoStorage(final StorageUrlProperties urlProperties) {
        this.videoStorageUrl = urlProperties.getVideoUrl();
    }

    @Override
    public String save(final String url) {
        System.out.println("Video by url " + url + " saved in storage");
        System.out.println("Generated new URL for this video");

        final String videoId = UUID.randomUUID().toString();
        final String videoUrl = String.format("%s/%s", videoStorageUrl, videoId);
        System.out.println("New link: " + videoUrl);
        return videoUrl;
    }
}
