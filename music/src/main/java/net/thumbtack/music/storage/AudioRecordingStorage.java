package net.thumbtack.music.storage;

import net.thumbtack.music.config.StorageUrlProperties;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.UUID;

@Service("audioStorage")
public class AudioRecordingStorage implements DataStorage {
    private final String audioStorageUrl;

    @Autowired
    public AudioRecordingStorage(final StorageUrlProperties urlProperties) {
        this.audioStorageUrl = urlProperties.getAudioUrl();
    }

    @Override
    public String save(final String url) {
        System.out.println("Audio by url " + url + " saved in storage");
        System.out.println("Generated new URL for this audio");

        final String audioId = UUID.randomUUID().toString();
        final String audioUrl = String.format("%s/%s", audioStorageUrl, audioId);
        System.out.println("New link: " + audioUrl);
        return audioUrl;
    }
}
