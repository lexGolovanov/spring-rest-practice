package net.thumbtack.music.storage;

public interface DataStorage {
    String save(String url);
}
