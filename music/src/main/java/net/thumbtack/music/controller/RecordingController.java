package net.thumbtack.music.controller;

import net.thumbtack.music.dto.RecordingDto;
import net.thumbtack.music.service.RecordingService;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/storage")
public class RecordingController {
    private RecordingService recordingService;

    @Autowired
    public RecordingController(final RecordingService recordingService) {
        this.recordingService = recordingService;
    }

    @PostMapping(
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public RecordingDto saveRecording(@RequestBody @Valid RecordingDto recording) {
        return recordingService.save(recording);
    }
}
