package net.thumbtack.music.controller;

import net.thumbtack.music.dto.PublicationDtoRequest;
import net.thumbtack.music.dto.PublicationDtoResponse;
import net.thumbtack.music.service.PublicationService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/channels")
public class PublicationController {
    private final PublicationService publicationService;

    @Autowired
    public PublicationController(PublicationService publicationService) {
        this.publicationService = publicationService;
    }

    @PostMapping(
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    public PublicationDtoResponse publishTrack(@RequestBody @Valid PublicationDtoRequest publicationDto) {
        return publicationService.publish(publicationDto);
    }

    @DeleteMapping("/{publication}")
    public void deletePublication(@PathVariable("publication") String id) {
        publicationService.unpublish(id);
    }
}
