package net.thumbtack.music.controller;

import net.thumbtack.music.exception.ExceptionDtoResponse;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class GlobalExceptionHandler {
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ExceptionDtoResponse handleValidationException(MethodArgumentNotValidException exc) {
        final List<String> errorList = new ArrayList<>();

        exc.getBindingResult()
                .getGlobalErrors()
                .forEach(globalError -> errorList.add(
                        String.format("Object %s has error: %s",
                                globalError.getObjectName(),
                                globalError.getDefaultMessage()
                        ))
                );

        exc.getBindingResult()
                .getFieldErrors()
                .forEach(fieldError -> errorList.add(
                        String.format("Field \"%s\" %s, but has value: %s",
                                fieldError.getField(),
                                fieldError.getDefaultMessage(),
                                fieldError.getRejectedValue()
                        ))
                );
        return new ExceptionDtoResponse(errorList);
    }
}
