package net.thumbtack.music.controller;

import net.thumbtack.music.dto.PromotionDto;
import net.thumbtack.music.service.promotion.PromotionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/track")
public class PromotionController {
    private final PromotionService promotionService;

    @Autowired
    public PromotionController(final PromotionService promotionService) {
        this.promotionService = promotionService;
    }

    @PostMapping(
            value = "/{track}/promo",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public PromotionDto startCampaign(@PathVariable("track") String trackId) {
        return promotionService.startCampaign(trackId);
    }

    @PutMapping("/{track}/promo/{campaign}")
    public void stopCampaign(@PathVariable("track") String trackId,
                             @PathVariable("campaign") String campaignId) {
        promotionService.stopCampaign(trackId, campaignId);
    }

    @DeleteMapping("/{track}/promo/{campaign}")
    public void deleteCampaign(@PathVariable("track") String trackId,
                               @PathVariable("campaign") String campaignId) {
        promotionService.deleteCampaign(trackId, campaignId);
    }
}
