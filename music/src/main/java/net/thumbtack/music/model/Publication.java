package net.thumbtack.music.model;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Publication {
    private String id;
    private Recording recording;
    private ZonedDateTime publicationDate;
    private List<String> publishedChannels;

    public Publication(String id, Recording recording,
                       ZonedDateTime publicationDate, List<String> publishedChannels) {
        this.id = id;
        this.recording = recording;
        this.publicationDate = publicationDate;
        this.publishedChannels = publishedChannels;
    }

    public Publication(String id, Recording recording, ZonedDateTime publicationDate) {
        this(id, recording, publicationDate, new ArrayList<>());
    }

    public String getId() {
        return id;
    }

    public Recording getRecording() {
        return recording;
    }

    public ZonedDateTime getPublicationDate() {
        return publicationDate;
    }

    public List<String> getPublishedChannels() {
        return publishedChannels;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Publication)) return false;
        Publication that = (Publication) o;
        return Objects.equals(getId(), that.getId()) &&
                Objects.equals(getRecording(), that.getRecording()) &&
                Objects.equals(getPublicationDate(), that.getPublicationDate()) &&
                Objects.equals(getPublishedChannels(), that.getPublishedChannels());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getRecording(), getPublicationDate(), getPublishedChannels());
    }
}
