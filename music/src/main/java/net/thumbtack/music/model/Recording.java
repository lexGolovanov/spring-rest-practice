package net.thumbtack.music.model;

import java.util.Objects;

public class Recording {
    private final String artist;
    private final String songTitle;
    private final String albumTitle;
    private final int year;
    private final String albumCoverUrl;
    private final String genre;
    private final String length;
    private final String audioUrl;
    private final String videoUrl;

    public Recording(String artist, String songTitle, String albumTitle, int year,
                     String albumCoverUrl, String genre, String length,
                     String audioUrl, String videoUrl) {
        this.artist = artist;
        this.songTitle = songTitle;
        this.albumTitle = albumTitle;
        this.year = year;
        this.albumCoverUrl = albumCoverUrl;
        this.genre = genre;
        this.length = length;
        this.audioUrl = audioUrl;
        this.videoUrl = videoUrl;
    }

    public String getArtist() {
        return artist;
    }

    public String getSongTitle() {
        return songTitle;
    }

    public String getAlbumTitle() {
        return albumTitle;
    }

    public int getYear() {
        return year;
    }

    public String getAlbumCoverUrl() {
        return albumCoverUrl;
    }

    public String getGenre() {
        return genre;
    }

    public String getLength() {
        return length;
    }

    public String getAudioUrl() {
        return audioUrl;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Recording)) return false;
        Recording recording = (Recording) o;
        return getYear() == recording.getYear() &&
                Objects.equals(getArtist(), recording.getArtist()) &&
                Objects.equals(getSongTitle(), recording.getSongTitle()) &&
                Objects.equals(getAlbumTitle(), recording.getAlbumTitle()) &&
                Objects.equals(getAlbumCoverUrl(), recording.getAlbumCoverUrl()) &&
                Objects.equals(getGenre(), recording.getGenre()) &&
                Objects.equals(getLength(), recording.getLength()) &&
                Objects.equals(getAudioUrl(), recording.getAudioUrl()) &&
                Objects.equals(getVideoUrl(), recording.getVideoUrl());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getArtist(), getSongTitle(), getAlbumTitle(), getYear(),
                getAlbumCoverUrl(), getGenre(), getLength(), getAudioUrl(), getVideoUrl()
        );
    }
}
