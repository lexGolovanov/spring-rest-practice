package net.thumbtack.music.service;

import net.thumbtack.music.model.Recording;
import net.thumbtack.music.dto.RecordingDto;
import net.thumbtack.music.converter.RecordingConverter;
import net.thumbtack.music.service.data.RecordingDataHub;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

@Service("recordingService")
public class RecordingService {
    private final RecordingDataHub recordingDataHub;
    private final RecordingConverter recordingConverter;

    @Autowired
    public RecordingService(final RecordingDataHub recordingDataHub,
                            final RecordingConverter recordingConverter) {
        this.recordingDataHub = recordingDataHub;
        this.recordingConverter = recordingConverter;
    }

    public RecordingDto save(final RecordingDto recording) {
        System.out.println("Converting recording DTO to recording object");
        Recording recordingModel = recordingConverter.recordingDtoToRecording(recording);
        recordingModel = recordingDataHub.save(recordingModel);

        System.out.println("Converting recording to recording DTO");
        return recordingConverter.recordingToRecordingDto(recordingModel);
    }
}
