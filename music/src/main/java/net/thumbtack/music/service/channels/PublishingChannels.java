package net.thumbtack.music.service.channels;

import net.thumbtack.music.model.Publication;

public interface PublishingChannels {
    void publish(Publication publication);
}
