package net.thumbtack.music.service;

import net.thumbtack.music.dto.PublicationDtoRequest;
import net.thumbtack.music.dto.PublicationDtoResponse;
import net.thumbtack.music.model.Publication;
import net.thumbtack.music.model.Recording;
import net.thumbtack.music.service.channels.PublishingChannels;
import net.thumbtack.music.service.channels.ItunesChannel;
import net.thumbtack.music.service.channels.YandexMusicChannel;
import net.thumbtack.music.service.channels.YoutubeMusicChannel;
import net.thumbtack.music.converter.RecordingConverter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class PublicationService {
    private final RecordingConverter recordingConverter;
    private final ItunesChannel itunesChannel;
    private final YoutubeMusicChannel youtubeMusicChannel;
    private final YandexMusicChannel yandexMusicChannel;
    private final Map<String, PublishingChannels> channelsMap;

    @Autowired
    public PublicationService(final ItunesChannel itunesChannel,
                              final YoutubeMusicChannel youtubeMusicChannel,
                              final YandexMusicChannel yandexMusicChannel,
                              final RecordingConverter recordingConverter) {
        this.itunesChannel = itunesChannel;
        this.youtubeMusicChannel = youtubeMusicChannel;
        this.yandexMusicChannel = yandexMusicChannel;
        this.recordingConverter = recordingConverter;

        channelsMap = new HashMap<>();
        channelsMap.put("itunes", itunesChannel);
        channelsMap.put("youtubemusic", youtubeMusicChannel);
        channelsMap.put("yandexmusic", yandexMusicChannel);
    }

    public PublicationDtoResponse publish(final PublicationDtoRequest publicationDto) {
        System.out.println("Converting recording DTO to recording object");
        final Recording recording = recordingConverter.recordingDtoToRecording(publicationDto.getRecording());

        System.out.println("Creating publication model");
        final Publication publication = new Publication(
                UUID.randomUUID().toString(),
                recording,
                publicationDto.getPublishingDate()
        );

        for (String channelName : publicationDto.getPublicationChannels()) {
            final PublishingChannels channel = channelsMap.get(channelName);
            if (channel != null) {
                channel.publish(publication);
            }
        }
        return new PublicationDtoResponse(publication.getId());
    }

    public void unpublish(final String publicationId) {
        System.out.println("Deleting publication by id " + publicationId);
    }
}
