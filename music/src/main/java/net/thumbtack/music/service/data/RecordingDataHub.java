package net.thumbtack.music.service.data;

import net.thumbtack.music.model.Recording;
import net.thumbtack.music.storage.VideoStorage;
import net.thumbtack.music.storage.AudioRecordingStorage;

import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

@Component("recordingDataHub")
public class RecordingDataHub {
    private final AudioRecordingStorage audioRecordingStorage;
    private final VideoStorage videoStorage;

    @Autowired
    public RecordingDataHub(final AudioRecordingStorage var1,
                            final VideoStorage var2) {
        this.audioRecordingStorage = var1;
        this.videoStorage = var2;
    }

    private String saveAudio(final String audioUrl) {
        if (audioUrl == null) {
            System.out.println("No audio URL found");
            return null;
        }
        System.out.println("DataHub preparing audio to save");
        return audioRecordingStorage.save(audioUrl);
    }

    private String saveVideo(final String videoUrl) {
        if (videoUrl == null) {
            System.out.println("No video URL found");
            return null;
        }
        System.out.println("DataHub preparing video to save");
        return videoStorage.save(videoUrl);
    }

    public Recording save(final Recording recording) {
        System.out.println("Received record for save");
        final String audioUrl = saveAudio(recording.getAudioUrl());
        final String videoUrl = saveVideo(recording.getVideoUrl());

        System.out.println("Saved record in storage");
        return new Recording(
                recording.getArtist(),
                recording.getSongTitle(),
                recording.getAlbumTitle(),
                recording.getYear(),
                recording.getAlbumCoverUrl(),
                recording.getGenre(),
                recording.getLength(),
                audioUrl,
                videoUrl
        );
    }
}
