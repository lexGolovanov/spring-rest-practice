package net.thumbtack.music.service.promotion;

import net.thumbtack.music.dto.PromotionDto;

import org.springframework.stereotype.Service;

import java.util.UUID;

@Service("promotionService")
public class PromotionService {
    public PromotionDto startCampaign(final String trackId) {
        final String campaignId = UUID.randomUUID().toString();
        System.out.println(
                String.format("Campaign by ID %s for recording with ID %s started",
                        campaignId, trackId
                )
        );
        return new PromotionDto(campaignId);
    }

    public void stopCampaign(final String trackId, final String campaignId) {
        System.out.println(
                String.format("Campaign by ID %s for recording with ID %s stopped",
                        campaignId, trackId
                )
        );
    }

    public void deleteCampaign(final String trackId, final String campaignId) {
        System.out.println(
                String.format("Campaign by ID %s for recording with ID %s finished",
                        campaignId, trackId
                )
        );
    }
}
