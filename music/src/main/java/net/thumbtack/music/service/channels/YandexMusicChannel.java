package net.thumbtack.music.service.channels;

import net.thumbtack.music.model.Publication;
import net.thumbtack.music.model.Recording;

import org.springframework.stereotype.Service;

import java.time.ZonedDateTime;

@Service("yandexMusicChannel")
public class YandexMusicChannel implements PublishingChannels {
    @Override
    public void publish(final Publication publication) {
        final Recording record = publication.getRecording();
        final ZonedDateTime publicationDate = publication.getPublicationDate();

        if (record.getAudioUrl() != null) {
            System.out.println(
                    String.format("Published \"%s\" by %s on YandexMusic.\nDate: %s",
                            record.getSongTitle(), record.getArtist(), publicationDate.toString()
                    )
            );
        } else {
            System.out.println("Audio URL not found for this record. Ignored publishing on YandexMusic");
        }
    }
}
