package net.thumbtack.music.dto;

import net.thumbtack.music.validator.AtLeastOneLinkExists;
import net.thumbtack.music.validator.ReleaseYear;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@AtLeastOneLinkExists
public class RecordingDto {
    @NotBlank
    private String artist;
    @NotBlank
    private String songTitle;
    private String albumTitle;
    @NotNull
    @ReleaseYear(minYear = 1970)
    private int year;
    private String albumCoverUrl;
    @NotBlank
    private String genre;
    @NotBlank
    private String length;
    private String audioUrl;
    private String videoUrl;

    public RecordingDto() {
    }

    public RecordingDto(String artist, String songTitle, String albumTitle, int year,
                        String albumCoverUrl, String genre, String length,
                        String audioUrl, String videoUrl) {
        this.artist = artist;
        this.songTitle = songTitle;
        this.albumTitle = albumTitle;
        this.year = year;
        this.albumCoverUrl = albumCoverUrl;
        this.genre = genre;
        this.length = length;
        this.audioUrl = audioUrl;
        this.videoUrl = videoUrl;
    }

    public String getArtist() {
        return artist;
    }

    public String getSongTitle() {
        return songTitle;
    }

    public String getAlbumTitle() {
        return albumTitle;
    }

    public int getYear() {
        return year;
    }

    public String getAlbumCoverUrl() {
        return albumCoverUrl;
    }

    public String getGenre() {
        return genre;
    }

    public String getLength() {
        return length;
    }

    public String getAudioUrl() {
        return audioUrl;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RecordingDto)) return false;
        RecordingDto that = (RecordingDto) o;
        return getYear() == that.getYear() &&
                Objects.equals(getArtist(), that.getArtist()) &&
                Objects.equals(getSongTitle(), that.getSongTitle()) &&
                Objects.equals(getAlbumTitle(), that.getAlbumTitle()) &&
                Objects.equals(getAlbumCoverUrl(), that.getAlbumCoverUrl()) &&
                Objects.equals(getGenre(), that.getGenre()) &&
                Objects.equals(getLength(), that.getLength()) &&
                Objects.equals(getAudioUrl(), that.getAudioUrl()) &&
                Objects.equals(getVideoUrl(), that.getVideoUrl());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getArtist(), getSongTitle(), getAlbumTitle(), getYear(),
                getAlbumCoverUrl(), getGenre(), getLength(), getAudioUrl(), getVideoUrl()
        );
    }
}
