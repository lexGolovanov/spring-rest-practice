package net.thumbtack.music.dto;

public class PublicationDtoResponse {
    private String publicationId;

    public PublicationDtoResponse() {
    }

    public PublicationDtoResponse(String publicationId) {
        this.publicationId = publicationId;
    }

    public String getPublicationId() {
        return publicationId;
    }
}
