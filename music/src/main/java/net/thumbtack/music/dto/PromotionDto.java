package net.thumbtack.music.dto;

public class PromotionDto {
    private String promotionCampaignId;

    public PromotionDto() {
    }

    public PromotionDto(String promotionCampaignId) {
        this.promotionCampaignId = promotionCampaignId;
    }

    public String getPromotionCampaignId() {
        return promotionCampaignId;
    }
}
