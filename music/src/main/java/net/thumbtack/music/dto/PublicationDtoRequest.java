package net.thumbtack.music.dto;

import net.thumbtack.music.validator.CorrectChannelsNames;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Objects;

public class PublicationDtoRequest {
    private RecordingDto recording;
    @CorrectChannelsNames
    private List<String> publicationChannels;
    private ZonedDateTime publishingDate;

    public PublicationDtoRequest() {
    }

    public PublicationDtoRequest(RecordingDto recording,
                                 List<String> publicationChannels,
                                 ZonedDateTime publishingDate) {
        this.recording = recording;
        this.publicationChannels = publicationChannels;
        this.publishingDate = publishingDate;
    }

    public RecordingDto getRecording() {
        return recording;
    }

    public List<String> getPublicationChannels() {
        return publicationChannels;
    }

    public ZonedDateTime getPublishingDate() {
        return publishingDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PublicationDtoRequest)) return false;
        PublicationDtoRequest that = (PublicationDtoRequest) o;
        return Objects.equals(getRecording(), that.getRecording()) &&
                Objects.equals(getPublicationChannels(), that.getPublicationChannels()) &&
                Objects.equals(getPublishingDate(), that.getPublishingDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getRecording(), getPublicationChannels(), getPublishingDate());
    }
}
