package net.thumbtack.music.exception;

import java.util.ArrayList;
import java.util.List;

public class ExceptionDtoResponse {
    private List<String> errorList;

    public ExceptionDtoResponse() {
    }

    public ExceptionDtoResponse(List<String> errorList) {
        this.errorList = errorList;
    }

    public List<String> getErrorList() {
        return errorList;
    }
}
