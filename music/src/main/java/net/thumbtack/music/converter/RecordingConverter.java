package net.thumbtack.music.converter;

import net.thumbtack.music.dto.RecordingDto;
import net.thumbtack.music.model.Recording;
import org.springframework.stereotype.Component;

@Component("recordingConverter")
public class RecordingConverter {
    public Recording recordingDtoToRecording(final RecordingDto recordingDto) {
        return new Recording(
                recordingDto.getArtist(),
                recordingDto.getSongTitle(),
                recordingDto.getAlbumTitle(),
                recordingDto.getYear(),
                recordingDto.getAlbumCoverUrl(),
                recordingDto.getGenre(),
                recordingDto.getLength(),
                recordingDto.getAudioUrl(),
                recordingDto.getVideoUrl()
        );
    }

    public RecordingDto recordingToRecordingDto(final Recording recording) {
        return new RecordingDto(
                recording.getArtist(),
                recording.getSongTitle(),
                recording.getAlbumTitle(),
                recording.getYear(),
                recording.getAlbumCoverUrl(),
                recording.getGenre(),
                recording.getLength(),
                recording.getAudioUrl(),
                recording.getVideoUrl()
        );
    }
}
