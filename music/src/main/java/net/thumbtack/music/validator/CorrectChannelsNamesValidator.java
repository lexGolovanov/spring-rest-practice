package net.thumbtack.music.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Arrays;
import java.util.List;

public class CorrectChannelsNamesValidator implements ConstraintValidator<CorrectChannelsNames, List<String>> {
    private List<String> enabledChannelsNames;

    @Override
    public void initialize(CorrectChannelsNames constraintAnnotation) {
        enabledChannelsNames = Arrays.asList("itunes", "youtubemusic", "yandexmusic");
    }

    @Override
    public boolean isValid(List<String> strings, ConstraintValidatorContext constraintValidatorContext) {
        return enabledChannelsNames.containsAll(strings);
    }
}
