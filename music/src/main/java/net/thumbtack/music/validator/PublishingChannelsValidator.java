package net.thumbtack.music.validator;

import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class PublishingChannelsValidator {
    private List<String> enabledChannels = Arrays.asList("iTunes", "YandexMusic", "youtubeMusic");

    public boolean validate(final List<String> channels) {
        for (String channel : channels) {
            for (String enabledChannel : enabledChannels) {
                if (!channel.equalsIgnoreCase(enabledChannel)) {
                    return false;
                }
            }
        }
        return true;
    }
}
