package net.thumbtack.music.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = ReleaseYearValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ReleaseYear {
    String message() default "release year too low";
    int minYear();
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
