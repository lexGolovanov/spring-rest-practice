package net.thumbtack.music.validator;

import net.thumbtack.music.dto.RecordingDto;
import org.springframework.stereotype.Component;

@Component
public class LinkValidator {
    public boolean validate(final RecordingDto recording) {
        return recording.getAudioUrl() != null || recording.getVideoUrl() != null;
    }
}
