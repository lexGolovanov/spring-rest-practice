package net.thumbtack.music.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = CorrectChannelsNamesValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface CorrectChannelsNames {
    String message() default "not valid channel name found";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
