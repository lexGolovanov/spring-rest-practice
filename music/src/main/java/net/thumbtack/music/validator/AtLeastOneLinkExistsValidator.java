package net.thumbtack.music.validator;

import org.springframework.beans.BeanWrapperImpl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class AtLeastOneLinkExistsValidator implements ConstraintValidator<AtLeastOneLinkExists, Object> {
    @Override
    public boolean isValid(Object o, ConstraintValidatorContext constraintValidatorContext) {
        String audioUrlFieldValue = (String) new BeanWrapperImpl(o)
                .getPropertyValue("audioUrl");
        String videoUrlFieldValue = (String) new BeanWrapperImpl(o)
                .getPropertyValue("videoUrl");

        return audioUrlFieldValue != null || videoUrlFieldValue != null;
    }
}
