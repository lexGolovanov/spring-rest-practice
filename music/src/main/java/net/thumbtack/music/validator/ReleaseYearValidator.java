package net.thumbtack.music.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ReleaseYearValidator implements ConstraintValidator<ReleaseYear, Integer> {
    private int minYear;

    @Override
    public void initialize(ReleaseYear constraintAnnotation) {
        minYear = constraintAnnotation.minYear();
    }

    @Override
    public boolean isValid(Integer year, ConstraintValidatorContext constraintValidatorContext) {
        return year > minYear;
    }
}
