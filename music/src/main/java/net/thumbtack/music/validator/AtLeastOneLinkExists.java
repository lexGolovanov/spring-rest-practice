package net.thumbtack.music.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = AtLeastOneLinkExistsValidator.class)
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface AtLeastOneLinkExists {
    String message() default "no one link exists";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
